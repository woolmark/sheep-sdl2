#include <iostream>
#include <SDL.h>
#include <SDL_image.h>
#include <SDL_ttf.h>
#include <pthread.h>
#include <unistd.h>
#include <stdio.h>

#define nullptr NULL

static SDL_Renderer* ren;
static SDL_Window* win;
static SDL_Texture* sheepImg00;
static SDL_Texture* sheepImg01;
static SDL_Texture* backgroundImg;
static TTF_Font* font;
static const int SKY_COLOR = 0x9696ff;
static const int GROUND_COLOR = 0x64ff64;
static const int SHEEP_MAX = 100;
static const int WIDTH = 120;
static const int HEIGHT = 120;
static BOOL isClosed = false;
BOOL flag = false;

int sheepPos[SHEEP_MAX][4];
int sheepNumber;
int sheepState;

int getJumpX(int y) {
	y -= (HEIGHT - 40);
	return (70 - 3 * y / 4);
}

static void setSheepInitPosition(int* pos) {
	pos[0] = WIDTH - 20;
	pos[1] = HEIGHT - 35 + (rand() % 20);
	pos[2] = 0;
	pos[3] = getJumpX(pos[1]);
}

static void addSheep() {
	for (int i = 1; i < SHEEP_MAX; i++) {
		if (sheepPos[i][1] == -1) {
			setSheepInitPosition(sheepPos[i]);
			return;
		}
	}
}

void clearSheepPosition(int* pos) {
	pos[0] = 0;
	pos[1] = -1;
	pos[2] = 0;
	pos[3] = 0;
}

static void calc() {
	if (flag) {
		addSheep();
		flag = false;
	}

	for (int i = 0; i < SHEEP_MAX; i++) {
		if (sheepPos[i][1] >= 0) {
			sheepPos[i][0] -= 5;
			if ((sheepPos[i][3] - 20) < sheepPos[i][0]
					&& sheepPos[i][0] <= sheepPos[i][3]) {
				sheepPos[i][1] -= 3;
			}

			else {
				if ((sheepPos[i][3] - 40) < sheepPos[i][0]
						&& sheepPos[i][0] <= (sheepPos[i][3] - 20)) {
					sheepPos[i][1] += 3;
				}
				if (sheepPos[i][0] < (sheepPos[i][3] - 10)
						&& sheepPos[i][2] == 0) {
					sheepNumber++;
					sheepPos[i][2] = 1;
				}
			}

			if (sheepPos[i][0] < 0) {
				if (i == 0) {
					setSheepInitPosition(sheepPos[0]);
				} else {
					clearSheepPosition(sheepPos[i]);
				}
			}
		}
	}
	sheepState++;
}

void* threadMain(void* p) {

	while (!isClosed) {
		calc();
		usleep(60 * 1000);
	}
	return NULL;
}

SDL_Texture* loadTexture(const std::string &file, SDL_Renderer *ren) {
	SDL_Texture *texture = IMG_LoadTexture(ren, file.c_str());
	if (texture == nullptr) {
		std::cout << "LoadTexture error" << SDL_GetError() << std::endl;
	}
	return texture;
}

static void init(void) {
	pthread_t thread;
	sheepNumber = 1;

	for (int i = 0; i < SHEEP_MAX; i++) {
		clearSheepPosition(sheepPos[i]);
	}

	setSheepInitPosition(sheepPos[0]);
	pthread_create(&thread, NULL, threadMain, NULL);
	flag = false;
	sheepState = 0;

	if (SDL_Init(SDL_INIT_EVERYTHING) != 0) {
		std::cout << "SDL_Init Error: " << SDL_GetError() << std::endl;
	}

	win = SDL_CreateWindow("WoolMark", 100, 100, 120, 120, SDL_WINDOW_SHOWN);

	if (win == nullptr) {
		std::cout << "SDL_CreateWindow Error: " << SDL_GetError() << std::endl;
	}

	ren = SDL_CreateRenderer(win, -1,
			SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
	if (ren == nullptr) {
		std::cout << "SDL_CreateRenderer Error: " << SDL_GetError()
				<< std::endl;
	}

	if ((IMG_Init(IMG_INIT_PNG) & IMG_INIT_PNG) != IMG_INIT_PNG) {
		std::cout << "IMG_Init error" << SDL_GetError() << std::endl;
	}

	backgroundImg = loadTexture("res/fence.png", ren);
	sheepImg00 = loadTexture("res/sheep00.png", ren);
	sheepImg01 = loadTexture("res/sheep01.png", ren);

	TTF_Init();
	font = TTF_OpenFont("res/ipag-mona.ttf", 12);
	std::cout << TTF_GetError() << std::endl;
}

void drawResult() {
	SDL_Surface* image;
	char text[128];
	sprintf(text, "羊が %d 匹", sheepNumber);
	SDL_Color black = { 0, 0, 0 };
	image = TTF_RenderUTF8_Blended(font, text, black);
	SDL_Rect dst;
	dst.x = 5;
	dst.y = 15;
	dst.w = image->w;
	dst.h = image->h;

	SDL_Texture *tex = SDL_CreateTextureFromSurface(ren, image);

	SDL_RenderCopy(ren, tex, NULL, &dst);
}

void drawSheep(int i) {
	SDL_Rect dst;
	dst.x = sheepPos[i][0];
	dst.y = sheepPos[i][1];
	dst.w = 17;
	dst.h = 12;
	if (sheepState % 2 == 1) {
		SDL_RenderCopy(ren, sheepImg00, NULL, &dst);
	} else {
		SDL_RenderCopy(ren, sheepImg01, NULL, &dst);
	}
}

void drawFence() {
	SDL_Rect dst;
	dst.x = 35;
	dst.y = HEIGHT - 80;
	dst.w = 52;
	dst.h = 78;
	SDL_RenderCopy(ren, backgroundImg, NULL, &dst);
}

void drawBackGround() {
	SDL_Surface* screenSurface = SDL_GetWindowSurface(win);

	SDL_Rect dst;
	dst.x = 0;
	dst.y = 0;
	dst.w = WIDTH;
	dst.h = HEIGHT;
	SDL_FillRect(screenSurface, &dst, GROUND_COLOR);
	SDL_Texture *tex = SDL_CreateTextureFromSurface(ren, screenSurface);
	SDL_RenderCopy(ren, tex, NULL, &dst);

	dst.x = 0;
	dst.y = 0;
	dst.w = WIDTH;
	dst.h = HEIGHT - 45;
	SDL_FillRect(screenSurface, &dst, SKY_COLOR);
	tex = SDL_CreateTextureFromSurface(ren, screenSurface);
	SDL_RenderCopy(ren, tex, NULL, &dst);
}

void paint() {
	drawBackGround();
	drawFence();
	for (int i = 0; i < SHEEP_MAX; i++) {
		if (sheepPos[i][1] >= 0) {
			drawSheep(i);
		}
	}
	drawResult();
}

int main(int argc, char** argv) {
	init();

	SDL_Event e;
	bool quit = false;

	while (!quit) {
		while (SDL_PollEvent(&e)) {
			if (e.type == SDL_QUIT) {
				quit = true;
			}
			if (e.type == SDL_MOUSEBUTTONDOWN) {
				flag = true;
			} else if (e.type == SDL_MOUSEBUTTONUP) {
				flag = false;
			}

		}
		SDL_RenderClear(ren);
		paint();
		SDL_RenderPresent(ren);
	}

	SDL_DestroyRenderer(ren);
	SDL_DestroyWindow(win);
	SDL_Quit();
	return 0;
}
